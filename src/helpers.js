const fillInput = async (page, selector, value) => {
  try {
    console.log(`Filling ${selector} with ${value}`);
    await page.waitForSelector(selector, { timeout: 2000 });
    console.log(`Found ${selector}`);
    await page.focus(selector);
    console.log(`Focused ${selector}`);
    await page.keyboard.type(value);
    console.log(`Typed ${value}`);
  } catch (error) {
    console.error(`Can not fill input ${selector}`);
    console.error(error);
  }
};

module.exports = {
  fillInput,
}