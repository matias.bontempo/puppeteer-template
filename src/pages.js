const pages = {
  login: {
    path: 'admin/login.php',
    selectors: {
      input_email: 'input[name=email]',
      input_password: 'input[name=contrasenia]',
      btn_submit: '#loginForm button',
    },
  },
};

module.exports = pages;