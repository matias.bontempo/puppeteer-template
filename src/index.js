require('dotenv').config()
const puppeteer = require('puppeteer-core');

const pages = require('./pages');
const { fillInput } = require('./helpers');

const {
  CHROME_PATH,
  DEV,
  BASE_URL,
  VTA_USERNAME,
  VTA_PASSWORD,
} = process.env;

const login = (page) => {
  await page.goto(`${BASE_URL}/${pages.login.path}`);

  await fillInput(page, pages.login.selectors.input_email, VTA_USERNAME);
  await fillInput(page, pages.login.selectors.input_password, VTA_PASSWORD);

  await page.click(pages.login.selectors.btn_submit);

  await page.screenshot({ path: 'example.png' });
}

const init = async () => {
  const browser = await puppeteer.launch({
    executablePath: CHROME_PATH,
    headless: !DEV,
  });

  const page = await browser.newPage();

  await login(page);

  await browser.close();
}

init();